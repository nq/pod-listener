def RELEASE_IMAGE="${PROJECT_NAME}_release"
def APP_PRERELEASE="app-prerelease.registry.sdlc.paychex.com"
def DOCKER_RUN_ARGS="--rm -v /etc/localtime:/etc/localtime:ro -v \$WORKSPACE:/home/app -v \$WORKSPACE/.gradle:/home/app/.gradle -e LOCAL_USER_ID=\$(id -u \$USER)"
def GRADLE_BUILD_IMAGE="app-prerelease.registry.sdlc.paychex.com/tgeorge/gradle"
def OC_IMAGE="app-prerelease.registry.sdlc.paychex.com/tgeorge/oc"
def pullGradleImage = "docker pull ${GRADLE_BUILD_IMAGE}:latest"
def runGradleCommand = "docker run ${DOCKER_RUN_ARGS} ${GRADLE_BUILD_IMAGE}:latest gradle"
def buildReleaseImage = "docker build -f ${DOCKERFILE_PATH} -t ${RELEASE_IMAGE} ${DOCKER_BUILD_CONTEXT}"
def tagGitCommit = "docker tag ${RELEASE_IMAGE} ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG"
def pushGitCommit = "docker push ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG"
def imageTagVar = """cat << EOF > dockerTag.props
DOCKER_IMAGE_TAG=\${GIT_COMMIT:0:8}-\$(date +%m%d%y-%H-%M-%S)
EOF"""

def buildArtifacts = "build/libs/**"

def RELEASE_CONFIG_IMAGE="${PROJECT_NAME}_conf_release"
def buildReleaseConfigImage = "docker build -f ${CONFIG_DOCKERFILE_PATH} -t ${RELEASE_CONFIG_IMAGE} ${CONFIG_DOCKER_BUILD_CONTEXT}"
def tagGitCommitConfig = "docker tag ${RELEASE_CONFIG_IMAGE} ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${CONFIG_IMAGE_NAME}:\$DOCKER_IMAGE_TAG"
def pushGitCommitConfig = "docker push ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${CONFIG_IMAGE_NAME}:\$DOCKER_IMAGE_TAG"

def pullOcImage = "docker pull ${OC_IMAGE}:latest"
def deploy = "docker run ${DOCKER_RUN_ARGS} --env-file ./pipelines/$TARGET/config.env --env DOCKER_IMAGE_TAG=\$DOCKER_IMAGE_TAG --env DOCKER_REGISTRY=$DOCKER_REGISTRY --env IMAGE_NAMESPACE=$IMAGE_NAMESPACE --env IMAGE_NAME=$IMAGE_NAME --env CONFIG_IMAGE_NAME=$CONFIG_IMAGE_NAME --env OPENSHIFT_USERS=$OPENSHIFT_USERS ${OC_IMAGE}:latest bash /home/app/pipelines/$TARGET/deploy.sh"
def undeploy= "docker run ${DOCKER_RUN_ARGS} --env-file ./pipelines/$TARGET/config.env ${OC_IMAGE}:latest bash /home/app/pipelines/$TARGET/undeploy.sh"

// SB
// def verifyBuild = "ICD_openshift-image-certification_SB"
// RC
def verifyBuild = "ICD_openshift-image-certification_RC"

def dockerLabel="docker"

def gitOptions = {
    it / "gitTool" << "Git_Linux"
    it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "fastForwardMode" << "NO_FF"
    it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeRemote" << "origin"
    it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeTarget" << "${BRANCH_TO_MERGE_TO}"
    it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeStrategy" << "default"
}

def gitOptionsDefault = {
	it / "gitTool" << "Default"
	it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "fastForwardMode" << "NO_FF"
	it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeRemote" << "origin"
	it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeTarget" << "${BRANCH_TO_MERGE_TO}"
	it / "extensions" / "hudson.plugins.git.extensions.impl.PreBuildMerge" / "options" / "mergeStrategy" << "default"
}

def timestamp = {
    it / "buildWrappers" << "hudson.plugins.timestamper.TimestamperBuildWrapper"
}

def gitPublisher = {
        it / "publishers" / "hudson.plugins.git.GitPublisher" {
            configVersion "2"
            pushMerge "true"
            pushOnlyIfSuccess "true"
            forcePush "false"
            
        }
        it / "publishers" / "hudson.plugins.git.GitPublisher" / "tagsToPush" / "hudson.plugins.git.GitPublisher_-TagToPush"  {
            targetRepoName "origin"
            tagName "\${GIT_BRANCH}_\${GIT_COMMIT}_\$BUILD_NUMBER"
            createTag "true"
            updateTag "true"
        }
        it / "publishers" / "hudson.plugins.git.GitPublisher" / "branchesToPush" / "hudson.plugins.git.GitPublisher_-BranchToPush" {
            targetRepoName "origin"
            branchName "${PUSH_TARGET}"
        }
}

def removeContainers = {
    it / "publishers" / "com.nirima.jenkins.plugins.docker.publisher.DockerPublisherControl" {
        remove "true"
    }
}

def failureEmail = {
    extendedEmail(RECIPIENT_LIST) {
        trigger("Failure")
    }
}

Closure readyToDeployEmail(PREFIX, PROJECT_NAME, SUFFIX, DOCKER_REGISTRY, IMAGE_NAMESPACE, IMAGE_NAME) {
    return {
        extendedEmail(RECIPIENT_LIST) {
            trigger("Success")
            configure { node -> 
                node / defaultSubject << "${PREFIX}_${PROJECT_NAME}_${SUFFIX} is ready to deploy"
                node / defaultContent << """
                The project ${PREFIX}_${PROJECT_NAME}_${SUFFIX} has image ${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG that can be deployed to openshift.  
                <br>
                <br>
                You are most likely seeing this email because your pipeline is not currently configured to deploy to OpenShift.
                <br>
                <br>
                You can grab this image with the following command (on a machine that has docker):
                <br>
                <br>
                docker pull ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG
                
                """
            }
        }
    }
}

Closure deployedEmail(PREFIX, PROJECT_NAME, SUFFIX, DOCKER_REGISTRY, IMAGE_NAMESPACE, IMAGE_NAME, OPENSHIFT_ROUTE_URL) {
    return {
        extendedEmail(RECIPIENT_LIST) {
            trigger("Success")
            configure { node -> 
                node / defaultSubject << "${PREFIX}_${PROJECT_NAME}_${SUFFIX} has deployed a new instance of your app to OpenShift"
                node / defaultContent << """
                The project ${PREFIX}_${PROJECT_NAME}_${SUFFIX} has deployed image ${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG to OpenShift.
                <br>
                <br>
                Please navigate to <a href="${OPENSHIFT_ROUTE_URL}">${OPENSHIFT_ROUTE_URL}</a> to access the application.
                <br>
                <br>
                Note that this e-mail does not signify that the deployment has been successful, only that it has happened.
                <br>
                <br>
                You can grab this image locally with the following command (on a machine that has docker):
                <br>
                <br>
                docker pull ${DOCKER_REGISTRY}/${IMAGE_NAMESPACE}/${IMAGE_NAME}:\$DOCKER_IMAGE_TAG
                
                """
            }
        }
    } 
}

def copyBuildArtifacts = {
    it / "builders" / "hudson.plugins.copyartifact.CopyArtifact" {
        projectName "${PREFIX}_${PROJECT_NAME}_build_${SUFFIX}"
        filter ""
        target ""
        selector(class:"hudson.plugins.copyartifact.TriggeredBuildSelector") {
            fallbackToLastSuccessful "true"
        }
    }
}

def imageValidationBuilds = {
	it / "builders" << "hudson.plugins.parameterizedtrigger.TriggerBuilder" {
		configs {
			"hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig" {
				configs {
					"hudson.plugins.parameterizedtrigger.PredefinedBuildParameters" {
						properties "DOCKER_IMAGE_REPOSITORY=${IMAGE_NAMESPACE}/${IMAGE_NAME}\nDOCKER_IMAGE_TAG=\${DOCKER_IMAGE_TAG}"
					}
				}
				projects verifyBuild
				condition "ALWAYS"
				triggerWithNoParameters "false"
				block {
					buildStepFailureThreshold {
						name "FAILURE"
						ordinal "2"
						color "RED"
						completeBuild "true"
					}
					unstableThreshold {
						name "UNSTABLE"
						ordinal "1"
						color "YELLOW"
						completeBuild "true"
					}
					failureThreshold {
						name "FAILURE"
						ordinal "2"
						color "RED"
						completeBuild "true"
					}
				}
				buildAllNodesWithLabel "false"
			}
			"hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig" {
				configs {
					"hudson.plugins.parameterizedtrigger.PredefinedBuildParameters" {
						properties "DOCKER_IMAGE_REPOSITORY=${IMAGE_NAMESPACE}/${CONFIG_IMAGE_NAME}\nDOCKER_IMAGE_TAG=\${DOCKER_IMAGE_TAG}"
					}
				}
				projects verifyBuild
				condition "ALWAYS"
				triggerWithNoParameters "false"
				block {
					buildStepFailureThreshold {
						name "FAILURE"
						ordinal "2"
						color "RED"
						completeBuild "true"
					}
					unstableThreshold {
						name "UNSTABLE"
						ordinal "1"
						color "YELLOW"
						completeBuild "true"
					}
					failureThreshold {
						name "FAILURE"
						ordinal "2"
						color "RED"
						completeBuild "true"
					}
				}
				buildAllNodesWithLabel "false"
			}
		}
	}
}

def archiveLibsFolder = {            
    it / "publishers" / "hudson.tasks.ArtifactArchiver" {
        artifacts "${buildArtifacts}"
        allowEmptyArchive "false"
        onlyIfSuccessful "true"
        fingerprint "true"
        defaultExcludes "true"
        caseSensitive "true" 
    }
}

def defaultAuthorization = {
    permission("hudson.model.Item.Workspace:ENS Development")
    permission("hudson.model.Item.Read:ENS Development")
    permission("hudson.model.Item.Build:ENS Development")
    permission("hudson.model.Item.Configure:ENS Development")
    permission("hudson.model.Item.Delete:ENS Development")
}

def JOBS = [
    [
        name: "${PREFIX}_${PROJECT_NAME}_build_${SUFFIX}",
        displayName: "${PREFIX}_${PROJECT_NAME}_build_${SUFFIX}",
        description: "runs a build on ${PREFIX}_${PROJECT_NAME}",
		label: dockerLabel,
		triggers: [{
			if(SUFFIX.equals("nightly")) {
			   cron("@midnight")
			} else {
				scm("")
			}
		}],
        steps: [
            "${pullGradleImage}",
            "${runGradleCommand} clean build",
            "mkdir -p build/test-results && cd build/test-results && touch *.xml # getting a stale test report error see http://stackoverflow.com/questions/13879667/"
        ],
        publishers:
            [
                failureEmail,
                {
                    archiveJunit("build/test-results/*.xml") 
                },
                {
                    jacocoCodeCoverage {
                        classPattern "**/classes"
                        sourcePattern "**/src/main/java"
                        execPattern "**/**.exec"
                    }
                },
                {
                    findbugs('build/reports/findbugs/*.xml', false) {
                        canRunOnFailed true
                    }
                },
                {
                    checkstyle('build/reports/checkstyle/*.xml') {
                        canRunOnFailed true
                    }
                }
            ]
			
    ], [
        name: "${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}",
        displayName: "${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}",
        description: "Creates a release docker image and pushes it to the repository",
		label: dockerLabel,
        steps: [
            "${buildReleaseImage}",
            "${tagGitCommit}",
            "${pushGitCommit}"
        ],
        publishers: [
            failureEmail
        ]
    ], [
        name: "${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}",
        displayName: "${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}",
        description: "Deploys the image generated in the previous build to OpenShift",
		label: dockerLabel,
		parameters: [{stringParam("DOCKER_IMAGE_TAG")}],
        steps: []
    ]
]

if (ROLLOUT.equals("true")) {
	JOBS << [
		name: "${PREFIX}_${PROJECT_NAME}_verification_${SUFFIX}",
		displayName: "${PREFIX}_${PROJECT_NAME}_verification_${SUFFIX}",
		description: "Verifies docker images for application",
		label: dockerLabel,
		parameters: [{stringParam("DOCKER_IMAGE_TAG")}],
        publishers: [
            failureEmail
        ]
	]
}

JOBS.each { j -> 
    println "creating ${j.name}"
    job {
        name(j.name)
        description(j.description)
        displayName(j.displayName)
        label(j.label)
        authorization defaultAuthorization
		j.parameters.each { parameter ->
			parameters parameter
		}
        scm {
            git {
                remote {
                    name("origin")
                    url("${GIT_URL}")
                    branches("*/${GIT_BRANCH}")
                    // SB
                    // credentials("2c01e8af-7c21-4311-a6c8-2c5e31f9194b")
                    // RC
                    credentials("2141262a-c848-4612-bc37-74a51176b01e")
                }
                mergeOptions("origin", "${BRANCH_TO_MERGE_TO}")
                configure gitOptions
            }
        }
        j.triggers.each { trigger ->
            triggers trigger
        }
        steps {
			if (j.name == "${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}") {
				println "Configuring copying artifacts from build job"
				configure copyBuildArtifacts
				shell(imageTagVar)
				environmentVariables {propertiesFile("dockerTag.props")}
				
				println "Config image name = [${CONFIG_IMAGE_NAME}]"
				if (!"".equals(CONFIG_IMAGE_NAME)) {
					shell(buildReleaseConfigImage)
					shell(tagGitCommitConfig)
					shell(pushGitCommitConfig)
				}
			}
			
            j.steps.each { step ->
                shell(step)
            }
			
            if (j.name == "${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}") {
                if (DEPLOY.equals("true")) {
                    println "Configuring a deployment to OpenShift sandbox"
                    shell(pullOcImage)
                    shell(deploy)
                } else {
                    println "Configuring a dummy deployment"
                    shell("echo This is a dummy job")
                }
            }
        }
        j.publishers.each { publisher ->
            publishers publisher
        }
        configure timestamp
        publishers {			
			if (j.name == "${PREFIX}_${PROJECT_NAME}_build_${SUFFIX}") {
				println "Configuring archival of libs folder"
				configure archiveLibsFolder
				downstream("${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}")
			}
			
			if (j.name == "${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}") {
				downstreamParameterized {
					trigger("${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}") {
						parameters {
							predefinedProp ("DOCKER_IMAGE_TAG", "\${DOCKER_IMAGE_TAG}")
						}
					}
				}
				
				println "push back ${PUSH_BACK}"
				if (PUSH_BACK.equals("true")) {
					println "Configuring git publisher"
					configure gitPublisher
				}
			}
			
			if (j.name == "${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}") {
				if (DEPLOY.equals("true")) {
					publishers deployedEmail(PREFIX, PROJECT_NAME, SUFFIX, DOCKER_REGISTRY, IMAGE_NAMESPACE, IMAGE_NAME, OPENSHIFT_ROUTE_URL)
				} else {
					publishers readyToDeployEmail(PREFIX, PROJECT_NAME, SUFFIX, DOCKER_REGISTRY, IMAGE_NAMESPACE, IMAGE_NAME)
				}
				
				if (ROLLOUT.equals("true")) {
					downstreamParameterized {
						trigger("${PREFIX}_${PROJECT_NAME}_verification_${SUFFIX}") {
							parameters {
								predefinedProp ("DOCKER_IMAGE_TAG", "\${DOCKER_IMAGE_TAG}")
							}
						}
					}
				}
			}
			
			configure removeContainers
        }
		
		if (j.name == "${PREFIX}_${PROJECT_NAME}_verification_${SUFFIX}") {
			configure imageValidationBuilds
		}
    }
}

if (DEPLOY.equals("true")) {
    job {
        name("${PREFIX}_${PROJECT_NAME}_undeploy_${SUFFIX}")
        description("Undeploys ${PREFIX}_${PROJECT_NAME}_undeploy_${SUFFIX}")
        displayName("${PREFIX}_${PROJECT_NAME}_undeploy_${SUFFIX}")
        label dockerLabel
        authorization defaultAuthorization
        scm {
            git {
                remote {
                    name("origin")
                    url("${GIT_URL}")
                    branches("*/${GIT_BRANCH}")
                    // SB
                    // credentials("2c01e8af-7c21-4311-a6c8-2c5e31f9194b")
                    // RC
                    credentials("2141262a-c848-4612-bc37-74a51176b01e")
                }
                mergeOptions("origin", "${BRANCH_TO_MERGE_TO}")
                configure gitOptions
                // clean()
            }
        }
        steps {
            shell(pullOcImage)
            shell(undeploy)
        }
    }
}