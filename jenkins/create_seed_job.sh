#!/bin/bash

function usage() 
{
	cat <<-EOF
		This task requires a single argument, target, to tell gradle which pipeline to create.  

		For example, if you want to create a nightly pipeline, you would make a folder named 'nightly' underneath the pipelines folder, containing the pipeline metadata in the deployment_pipeline.properties file.

		Usage:

		./gradlew createPipeline -Ptarget=<target>

		Example:

		- Create a folder called 'nightly' underneath the pipelines folder.
		
		pipelines
		|__nightly

		- Create a file called deployment_pipeline.properties and add your metadata to it.

		pipelines
		|__nightly
		   |__deployment_pipeline.properties
			  
			   DOCKER_REGISTRY=app-prerelease.registry.sdlc.paychex.com
			   GIT_BRANCH=add_pipeline
			   GIT_URL=ssh://git@code.paychex.com/~tgeorge/bank-data-service.git
			   BRANCH_TO_MERGE_TO=master
			   IMAGE_NAME=bank_data_service
			   IMAGE_NAMESPACE=tgeorge
			   JENKINS_HOST=hcntpnv03.paychex.com:8080
			   OPENSHIFT_PROJECT=scrumlords
			   OPENSHIFT_USERS=tgeorge,aspooner,jprice3,cjordan1,jeklund1
			   PUSH_TARGET=master
			   PUSH_BACK=true
			   PREFIX=tgeorge
			   PROJECT_NAME=bank_data_service
			   RECIPIENT_LIST=tgeorge@paychex.com,aspooner@paychex.com

		- Run the gradle task passing in your target:
		./gradlew createPipeline -Ptarget=nightly
		
		For more information, see http://wiki.paychex.com/display/ENS/Creating+a+build+and+deploy+pipeline
	EOF
	exit 1
}

rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

TARGET=$1
if [[ -z $TARGET ]]; then
	usage
fi

source $(pwd)/pipelines/$TARGET/deployment_pipeline.properties

# Set default values
SUFFIX=${SUFFIX:-master} 
JENKINS_HOST=${JENKINS_HOST:-hcntpnv03.paychex.com:8080}
GIT_BRANCH=${GIT_BRANCH:-dmz}
BRANCH_TO_MERGE_TO=${BRANCH_TO_MERGE_TO:-master}
PUSH_BACK=${PUSH_BACK:-true}
PUSH_TARGET=${PUSH_TARGET:-master}
DOCKER_REGISTRY=${DOCKER_REGISTRY:-app-prerelease.registry.sdlc.paychex.com}
DOCKERFILE_PATH=${DOCKERFILE_PATH:-docker/app/release/Dockerfile}
DOCKER_BUILD_CONTEXT=${DOCKER_BUILD_CONTEXT:-.}
CONFIG_DOCKERFILE_PATH=${CONFIG_DOCKERFILE_PATH:-docker/app/release/Config-Dockerfile}
CONFIG_DOCKER_BUILD_CONTEXT=${CONFIG_DOCKER_BUILD_CONTEXT:-.}
DEPLOY=${DEPLOY:-false}
OPENSHIFT_HOST=${OPENSHIFT_HOST:-https://ose-wdc-sandbox.paas.paychex.com:8443}
CONTAINER_PORT=${CONTAINER_PORT:-80}
TARGET_PORT=${TARGET_PORT:-8080}
NUMBER_OF_REPLICAS=${NUMBER_OF_REPLICAS:-1}
ENV=${ENV:-sandbox}
ROLLOUT=${ROLLOUT:-false}
CODE_ANALYSIS=${CODE_ANALYSIS:-false}
TAF_GATEWAY=${TAF_GATEWAY:-false}

cat << EOF > pipelines/$TARGET/config.xml
<?xml version='1.0' encoding='UTF-8'?>
<project>
    <actions/>
    <description></description>
    <keepDependencies>false</keepDependencies>
    <properties>
    <hudson.security.AuthorizationMatrixProperty>
        <permission>hudson.model.Item.Delete:authenticated</permission>
        <permission>hudson.model.Item.Read:authenticated</permission>
        <permission>hudson.model.Item.Workspace:authenticated</permission>
        <permission>hudson.model.Item.Build:authenticated</permission>
        <permission>hudson.model.Item.Configure:authenticated</permission>
    </hudson.security.AuthorizationMatrixProperty>
    <com.sonyericsson.rebuild.RebuildSettings plugin="rebuild@1.22">
        <autoRebuild>false</autoRebuild>
    </com.sonyericsson.rebuild.RebuildSettings>
    <com.synopsys.arc.jenkinsci.plugins.jobrestrictions.jobs.JobRestrictionProperty plugin="job-restrictions@0.3"/>
    <hudson.model.ParametersDefinitionProperty>
        <parameterDefinitions>
        <hudson.model.StringParameterDefinition>
            <name>PREFIX</name>
            <description>The EBA agreed-upon prefix for your group&apos;s jenkins projects (e.g. ENS)</description>
            <defaultValue>$PREFIX</defaultValue>
        </hudson.model.StringParameterDefinition>        
        <hudson.model.StringParameterDefinition>
            <name>PROJECT_NAME</name>
            <description>Your jenkins project name.  Use underscores (e.g. angular2_starter)</description>
            <defaultValue>$PROJECT_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>SUFFIX</name>
            <description>A suffix value that gets added onto your build.  Helps determine what it does. e.g.  'master' will not deploy, 'nightly' deploys at night, or your branch name.  Default value is master, for a master pipeline that does not deploy</description>
            <defaultValue>$SUFFIX</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>GIT_URL</name>
            <description>The URL of your git repository (e.g. ssh://git@code.paychex.com/ensp/angular2-starter.git)</description>
            <defaultValue>$GIT_URL</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>GIT_BRANCH</name>
            <description>The branch to build (e.g. dmz, feature_x, master)</description>
            <defaultValue>$GIT_BRANCH</defaultValue>
        </hudson.model.StringParameterDefinition>
		<hudson.model.StringParameterDefinition>
            <name>BRANCH_TO_MERGE_TO</name>
            <description>The branch that the above parameter (GIT_BRANCH) will be merged into before building (e.g. dmz -> master, feature_x -> master</description>
            <defaultValue>$BRANCH_TO_MERGE_TO</defaultValue>
        </hudson.model.StringParameterDefinition>
		<hudson.model.BooleanParameterDefinition>
          <name>PUSH_BACK</name>
          <description>True if you want to push back to some branch (A CI pipeline would want to push the commits that land on dmz to master, for example.  A nightly build wouldn&apos;t push anything back.</description>
          <defaultValue>$PUSH_BACK</defaultValue>
        </hudson.model.BooleanParameterDefinition>        
		<hudson.model.StringParameterDefinition>
            <name>PUSH_TARGET</name>
            <description>The branch to push back to, if you want to</description>
            <defaultValue>$PUSH_TARGET</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>RECIPIENT_LIST</name>
            <description>A comma-separated list of e-mail recipients.  (e.g. tgeorge@paychex.com,aspooner@paychex.com,kestler@paychex.com)</description>
            <defaultValue>$RECIPIENT_LIST</defaultValue>
        </hudson.model.StringParameterDefinition> 
        <hudson.model.StringParameterDefinition>
            <name>DOCKER_REGISTRY</name>
            <description>The docker registry URL to push/pull from (e.g. app-prerelease.registry.sdlc.paychex.com)</description>
            <defaultValue>$DOCKER_REGISTRY</defaultValue>
        </hudson.model.StringParameterDefinition>              
        <hudson.model.StringParameterDefinition>
            <name>IMAGE_NAMESPACE</name>
            <description>The namespace of your image.  For example, for the tgeorge/angular2_starter image, tgeorge would be the namespace.</description>
            <defaultValue>$IMAGE_NAMESPACE</defaultValue>
        </hudson.model.StringParameterDefinition>         
        <hudson.model.StringParameterDefinition>
            <name>IMAGE_NAME</name>
            <description>The name of the image.  Defaults to your git repository (e.g. angular2_starter)</description>
            <defaultValue>$IMAGE_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>DOCKERFILE_PATH</name>
            <description>Path to the Dockerfile you use to build your release image, including the Dockerfile (e.g. docker/app/release/Dockerfile, src/main/docker/Dockerfile.prod).  Default is docker/app/release/Dockerfile.</description>
            <defaultValue>$DOCKERFILE_PATH</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>DOCKER_BUILD_CONTEXT</name>
            <description>Path to the docker build context.  See https://docs.docker.com/engine/reference/commandline/build/.  Default is current directory (.) </description>
            <defaultValue>$DOCKER_BUILD_CONTEXT</defaultValue>
        </hudson.model.StringParameterDefinition>
		<hudson.model.StringParameterDefinition>
            <name>CONFIG_IMAGE_NAME</name>
            <description>The name of the config image.</description>
            <defaultValue>$CONFIG_IMAGE_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>
		<hudson.model.StringParameterDefinition>
            <name>CONFIG_DOCKERFILE_PATH</name>
            <description>Path to the config Dockerfile you use to build your release config image, including the Dockerfile (e.g. docker/app/release/Dockerfile, src/main/docker/Dockerfile.prod).  Default is docker/app/release/Config-Dockerfile.</description>
            <defaultValue>$CONFIG_DOCKERFILE_PATH</defaultValue>
        </hudson.model.StringParameterDefinition>
		<hudson.model.StringParameterDefinition>
            <name>CONFIG_DOCKER_BUILD_CONTEXT</name>
            <description>Path to the docker build context.  See https://docs.docker.com/engine/reference/commandline/build/.  Default is current directory (.) </description>
            <defaultValue>$CONFIG_DOCKER_BUILD_CONTEXT</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>ENV</name>
            <description>The selected environment to use for the config sidecar (e.g. sandbox, n2a, n2b).  Default is sandbox.</description>
            <defaultValue>$ENV</defaultValue>
        </hudson.model.StringParameterDefinition>              
        <hudson.model.BooleanParameterDefinition>
          <name>DEPLOY</name>
          <description>True to deploy to OpenShift sandbox. Defaults to false.</description>
          <defaultValue>$DEPLOY</defaultValue>
        </hudson.model.BooleanParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_PROJECT</name>
            <description>The openshift project that your deployment will live in (e.g. Scrumlords)</description>
            <defaultValue>$OPENSHIFT_PROJECT</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_HOST</name>
            <description>The openshift URL with port to deploy to.  Default is Webster sandbox</description>
            <defaultValue>$OPENSHIFT_HOST</defaultValue>
        </hudson.model.StringParameterDefinition>        
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_USERS</name>
            <description>A comma-separated list of users who should have access to this openshift project (e.g. tgeorge,aspooner,jeklund1,cjordan1,jprice3)</description>
            <defaultValue>$OPENSHIFT_USERS</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_DEPLOYMENT_NAME</name>
            <description>The name of your openshift deployment configuration</description>
            <defaultValue>$OPENSHIFT_DEPLOYMENT_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>  
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_SERVICE_NAME</name>
            <description>The name of your OpenShift service associated with the deploymentConfig</description>
            <defaultValue>$OPENSHIFT_SERVICE_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_ROUTE_NAME</name>
            <description>The name of the OpenShift route associated with your deployment</description>
            <defaultValue>$OPENSHIFT_ROUTE_NAME</defaultValue>
        </hudson.model.StringParameterDefinition>       
        <hudson.model.StringParameterDefinition>
            <name>OPENSHIFT_ROUTE_URL</name>
            <description>The URL that you will use to access your appt</description>
            <defaultValue>$OPENSHIFT_ROUTE_URL</defaultValue>
        </hudson.model.StringParameterDefinition>       
        <hudson.model.StringParameterDefinition>
            <name>CONTAINER_PORT</name>
            <description>The port you will use to access the container (typically port 80 for a webapp). This name is confusing, but the deployment config uses this terminology.</description>
            <defaultValue>$CONTAINER_PORT</defaultValue>
        </hudson.model.StringParameterDefinition>               
        <hudson.model.StringParameterDefinition>
            <name>TARGET_PORT</name>
            <description>The port that your service is listening on inside the container.</description>
            <defaultValue>$TARGET_PORT</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>NUMBER_OF_REPLICAS</name>
            <description>The number of replicas of your deployment you want</description>
            <defaultValue>$NUMBER_OF_REPLICAS</defaultValue>
        </hudson.model.StringParameterDefinition>      
        <hudson.model.StringParameterDefinition>
            <name>TARGET</name>
            <description>The path to your pipeline config folder (pipelines/*).  Used to tell the deploy script which env file to use.</description>
            <defaultValue>$TARGET</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.BooleanParameterDefinition>
          <name>ROLLOUT</name>
          <description>True to rollout to xld. Defaults to false.</description>
          <defaultValue>$ROLLOUT</defaultValue>
        </hudson.model.BooleanParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>XLD_APPLICATION_PATH</name>
            <description>The application path in xld for the deployment. Should be similar to Applications/ens/openshift/ens-achapi.</description>
            <defaultValue>$XLD_APPLICATION_PATH</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>XLD_OPENSHIFT_PROJECT</name>
            <description>The openshift project name used by xld.</description>
            <defaultValue>$XLD_OPENSHIFT_PROJECT</defaultValue>
        </hudson.model.StringParameterDefinition>
        <hudson.model.BooleanParameterDefinition>
          <name>CODE_ANALYSIS</name>
          <description>True create and run code analysis jobs also (fortify and sonar). Defaults to false.</description>
          <defaultValue>$CODE_ANALYSIS</defaultValue>
        </hudson.model.BooleanParameterDefinition>
        <hudson.model.StringParameterDefinition>
            <name>FORTIFY_ID</name>
            <description>Fortify Project ID for reporting.</description>
            <defaultValue>$FORTIFY_ID</defaultValue>
        </hudson.model.StringParameterDefinition>
        </parameterDefinitions>
    </hudson.model.ParametersDefinitionProperty>
    <org.jvnet.hudson.plugins.shelveproject.ShelveProjectProperty plugin="shelve-project-plugin@1.5"/>
  </properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.5.2">
    <configVersion>2</configVersion>
    <userRemoteConfigs class="java.util.Collections\$UnmodifiableRandomAccessList" resolves-to="java.util.Collections\$UnmodifiableList">
      <c class="list">
        <hudson.plugins.git.UserRemoteConfig>
          <url>$GIT_URL</url>
          <credentialsId>2141262a-c848-4612-bc37-74a51176b01e</credentialsId>
        </hudson.plugins.git.UserRemoteConfig>
      </c>
      <list reference="../c"/>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/$GIT_BRANCH</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <gitTool>Git_Linux</gitTool>
    <extensions/>
  </scm>
  <assignedNode>docker</assignedNode>
  <canRoam>false</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <jdk>(System)</jdk>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <javaposse.jobdsl.plugin.ExecuteDslScripts plugin="job-dsl@1.23">
      <targets>jenkins/jobs/seed.groovy</targets>
      <usingScriptText>false</usingScriptText>
      <ignoreExisting>false</ignoreExisting>
      <removedJobAction>IGNORE</removedJobAction>
    </javaposse.jobdsl.plugin.ExecuteDslScripts>
  </builders>
  <publishers/>
  <buildWrappers/>
</project>
EOF

CREATE_JOB_URL="http://$JENKINS_USERNAME:$(rawurlencode "$JENKINS_PASSWORD" )@$JENKINS_HOST/createItem?name=${PREFIX}_${PROJECT_NAME}_seed_${SUFFIX}"
JOB_BUILD_URL="http://$JENKINS_USERNAME:$(rawurlencode "$JENKINS_PASSWORD")@$JENKINS_HOST/job/${PREFIX}_${PROJECT_NAME}_seed_${SUFFIX}"
ECHO_URL=$(echo $JOB_BUILD_URL | sed -e 's/[a-zA-Z0-9]*:.*@//g')
echo $ECHO_URL
# curl to create seed job
curl -s --fail -X GET $JOB_BUILD_URL -H "Content-Type:application/xml"
JOB_EXISTS=$?
if [[ $JOB_EXISTS -eq 22 ]]; then 
    curl -X POST -d @pipelines/$TARGET/config.xml $CREATE_JOB_URL -H "Content-Type:text/xml"
    [ $? -eq 0 ] && echo "Seed job created at $ECHO_URL"
    echo "Commit and push the pipelines/$TARGET directory to the branch $GIT_BRANCH so the job can see the pipeline files.  Then, run the seed job."
#	echo "Building seed job"
#	curl -X POST "$JOB_BUILD_URL/buildWithParameters"
else 
    echo "Job already exists.  Not creating again."
fi
