function usage() 
{
	cat <<-EOF

		This task requires a single argument, target, to tell gradle which pipeline to destroy.
		
		./gradlew destroyPipeline -Ptarget=<folder name of pipeline you want to destroy>  

		For more information, see http://wiki.paychex.com/display/ENS/Creating+a+build+and+deploy+pipeline
		
		
	EOF
	exit 1
}

TARGET=$1
if [[ -z $TARGET ]]; then
	usage
fi

source $(pwd)/pipelines/$TARGET/deployment_pipeline.properties
jobs=("${PREFIX}_${PROJECT_NAME}_xld_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_fortify_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_sonar_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_verification_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_deploy_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_tag_and_push_image_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_build_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_start_parallel_jobs_${SUFFIX}"
    "${PREFIX}_${PROJECT_NAME}_undeploy_${SUFFIX}"
	"${PREFIX}_${PROJECT_NAME}_seed_${SUFFIX}")

curl -s --fail -X POST "http://$JENKINS_USERNAME:$JENKINS_PASSWORD@${JENINS_HOST:-hcntpnv03.paychex.com:8080}/job/${PREFIX}_${PROJECT_NAME}_undeploy_${SUFFIX}/build" || [ $? -eq 22 ] && true
for job in "${jobs[@]}"
do
	DELETE_URL=http://$JENKINS_USERNAME:$JENKINS_PASSWORD@${JENKINS_HOST:-hcntpnv03.paychex.com:8080}/job/$job/doDelete 
	ECHO_URL=$(echo ${DELETE_URL} | sed -e 's/[a-zA-Z0-9]*:.*@//g')
	echo "Deleting job: $job - ${ECHO_URL}"
	curl -s --fail -X POST "${DELETE_URL}" || [ $? -eq 22 ] && continue
done

rm -f $(pwd)/pipelines/$TARGET/*.yml
rm -f $(pwd)/pipelines/$TARGET/*.json
rm -f $(pwd)/pipelines/$TARGET/xld/*.json
rm -f $(pwd)/pipelines/$TARGET/config.env
rm -f $(pwd)/pipelines/$TARGET/deploy.sh
rm -f $(pwd)/pipelines/$TARGET/prepareJson.sh
rm -f $(pwd)/pipelines/$TARGET/undeploy.sh
rm -f $(pwd)/pipelines/$TARGET/config.xml