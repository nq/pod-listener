#!/bin/bash

function usage()
{
	cat <<-EOF
		This task requires a single argument, target, to tell gradle which build you are triggering

		For example, if you want to create a nightly pipeline, you would make a folder named 'nightly' underneath the pipelines folder, containing the pipeline metadata in the deployment_pipeline.properties file.

		Usage:

		./gradlew createPipeline -Ptarget=<target>

		Example:

		- Create a folder called 'nightly' underneath the pipelines folder.

		pipelines
		|__nightly

		- Create a file called deployment_pipeline.properties and add your metadata to it.

		pipelines
		|__nightly
		   |__deployment_pipeline.properties

			   DOCKER_REGISTRY=app-prerelease.registry.sdlc.paychex.com
			   GIT_BRANCH=add_pipeline
			   GIT_URL=ssh://git@code.paychex.com/~tgeorge/bank-data-service.git
			   BRANCH_TO_MERGE_TO=master
			   IMAGE_NAME=bank_data_service
			   IMAGE_NAMESPACE=tgeorge
			   JENKINS_HOST=hcntpnv03.paychex.com:8080
			   OPENSHIFT_PROJECT=scrumlords
			   OPENSHIFT_USERS=tgeorge,aspooner,jprice3,cjordan1,jeklund1
			   PUSH_TARGET=master
			   PUSH_BACK=true
			   PREFIX=tgeorge
			   PROJECT_NAME=bank_data_service
			   RECIPIENT_LIST=tgeorge@paychex.com,aspooner@paychex.com

		- Run the gradle task passing in your target:
		./gradlew createPipeline -Ptarget=<target>

		For more information, see http://wiki.paychex.com/display/ENS/Creating+a+build+and+deploy+pipeline
	EOF
	exit 1
}

rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER)
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

TARGET=$1
if [[ -z $TARGET ]]; then
	usage
fi

source $(pwd)/pipelines/$TARGET/deployment_pipeline.properties

# Set default values
SUFFIX=${SUFFIX:-master}
JENKINS_HOST=${JENKINS_HOST:-hcntpnv03.paychex.com:8080}
GIT_BRANCH=${GIT_BRANCH:-dmz}
BRANCH_TO_MERGE_TO=${BRANCH_TO_MERGE_TO:-master}
PUSH_BACK=${PUSH_BACK:-true}
PUSH_TARGET=${PUSH_TARGET:-master}
DOCKER_REGISTRY=${DOCKER_REGISTRY:-app-prerelease.registry.sdlc.paychex.com}
DOCKERFILE_PATH=${DOCKERFILE_PATH:-docker/app/release/Dockerfile}
DOCKER_BUILD_CONTEXT=${DOCKER_BUILD_CONTEXT:-.}
CONFIG_DOCKERFILE_PATH=${CONFIG_DOCKERFILE_PATH:-docker/app/release/Config-Dockerfile}
CONFIG_DOCKER_BUILD_CONTEXT=${CONFIG_DOCKER_BUILD_CONTEXT:-.}
DEPLOY=${DEPLOY:-false}
OPENSHIFT_HOST=${OPENSHIFT_HOST:-https://ose-wdc-sandbox.paas.paychex.com:8443}
CONTAINER_PORT=${CONTAINER_PORT:-80}
TARGET_PORT=${TARGET_PORT:-8080}
NUMBER_OF_REPLICAS=${NUMBER_OF_REPLICAS:-1}
ENV=${ENV:-sandbox}

TRIGGER_BUILD_URL="http://$JENKINS_USERNAME:$(rawurlencode "$JENKINS_PASSWORD")@$JENKINS_HOST/job/${PREFIX}_${PROJECT_NAME}_start_parallel_jobs_${SUFFIX}/build"
JOB_BUILD_URL="http://$JENKINS_USERNAME:$(rawurlencode "$JENKINS_PASSWORD")@$JENKINS_HOST/job/${PREFIX}_${PROJECT_NAME}_seed_${SUFFIX}"
ECHO_URL=$(echo $JOB_BUILD_URL | sed -e 's/[a-zA-Z0-9]*:.*@//g')
echo "Running Jenkins Job ${PREFIX}_${PROJECT_NAME}_start_parallel_jobs_${SUFFIX}"
# curl to create seed job
curl -s --fail -X GET $JOB_BUILD_URL -H "Content-Type:application/xml"
JOB_EXISTS=$?
if [[ $JOB_EXISTS -eq 22 ]]; then
    echo "Job does not exist, not triggering a build."
#	echo "Building seed job"
else
	curl -X POST "$TRIGGER_BUILD_URL"
    echo "${PREFIX}_${PROJECT_NAME}_start_parallel_jobs_${SUFFIX} job running"
fi
