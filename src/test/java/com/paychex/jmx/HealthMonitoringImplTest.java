package com.paychex.jmx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mdrum
 *
 */
public class HealthMonitoringImplTest {

	private static Logger logger = LogManager
			.getLogger(HealthMonitoringImplTest.class);

	private HealthMonitoringImpl handler;

	@Before
	public void setUp() {
		handler = new HealthMonitoringImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGettersAndSetters() {
		String busyReason = "If you hot, you hot";
		String healthReason = "If you not, you not";

		handler.setBusy(true);
		handler.setBusyReason(busyReason);
		handler.setHealth(true);
		handler.setHealthReason(healthReason);

		logger.debug(handler.toString());

		assertTrue(handler.getBusy());
		assertEquals(handler.getBusyReason(), busyReason);
		assertTrue(handler.getHealth());
		assertEquals(handler.getHealthReason(), healthReason);
	}
}
