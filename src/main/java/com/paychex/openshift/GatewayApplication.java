package com.paychex.openshift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@SpringBootApplication
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
	
	@Bean
	public TaskExecutor taskExecutor() {
		// simple task executor for our threads
	    return new SimpleAsyncTaskExecutor();
	}

	@Bean
	public PodListenerService routeLocator(TaskExecutor taskExecutor) {
		return new PodListenerService(taskExecutor);
	}
}