package com.paychex.openshift.configuration;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jmx.export.MBeanExporter;

import com.paychex.jmx.HealthMonitoringImpl;

@Configuration
public class JmxConfig {

	@Bean
	public HealthMonitoringImpl healthMonitor() {
		HealthMonitoringImpl monitor = new HealthMonitoringImpl();
		monitor.setBusy(false);
		monitor.setHealth(true);
		return monitor;
	}
	
	@Bean
	@Lazy(false)
	public MBeanExporter getExporter() {
		MBeanExporter exporter = new MBeanExporter();
		Map<String, Object> beans = new HashMap<String, Object>();
		beans.put("com.paychex.jmx:type=HealthMonitoringMBean", healthMonitor());
		exporter.setBeans(beans);
	    return exporter;
	}
}
