package com.paychex.openshift;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;

import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.Watch;
import io.fabric8.kubernetes.client.Watcher;

/**
 * This services creates a mapping between the images used in the namespace (project?) and
 * a timer that counts down since when that image was last identified as being used in a container.
 * By default, the timer counts down from 180 days (~6 months).
 */
public class PodListenerService {

    private static int COUNTDOWN_DAYS = 180;

    @Value("${PAYX_KUBERNETES_NAMESPACE}")
    private String namespace;

    private Map<String, TimerTask> images = new ConcurrentHashMap<String, TimerTask>();

    private static final Logger logger = LoggerFactory.getLogger(PodListener.class);

    public PodListenerService(TaskExecutor executor) {
        executor.execute(new PodListener());
    }

    /**
     * Get the image names and the timers assigned to them.
     *
     * @return
     */
    public Map<String, TimerTask> getImages() {
        return images;
    }

    /**
     * The Listener thread that will be registered with the executor that is passed into the service.
     * It does polling for active pods currently being used (in case a pod is not changed in the 6 month
     * interval) and also watches for pod events for pods that are ephemeral (only exist for a very short
     * amount of time).
     */
    private class PodListener implements Runnable {
        /**
         * Main body of the Listener. Creates a watcher, and starts the poller.
         */
        @Override
        public void run() {
            Config config = new ConfigBuilder().build();
            Timer timer = new Timer(namespace);
            final KubernetesClient client = new DefaultKubernetesClient(config);

            // Create a watcher to listen for ephemeral jobs
            Watch watch = client.pods().inNamespace(namespace).withResourceVersion("0").watch(new Watcher<Pod>() {
                /**
                 * Called when an event is received for any pod in the namespace.
                 * @param action The @Action that is received (ADDED, MODIFIED, DELETED, ERROR). Ignored.
                 * @param resource The @Pod for which the event was received.
                 */
                @Override
                public void eventReceived(Action action, Pod resource) {
                    PodListener.this.updateImageTimer(timer, resource);
                }

                /**
                 * Called when the watcher is closed.
                 * @param e The exception that is thrown.
                 */
                @Override
                public void onClose(KubernetesClientException e) {
                    if (e != null) {
                        e.printStackTrace();
                        logger.error(e.getMessage(), e);
                        throw new RuntimeException(e);
                    }
                }
            });

            // Poll for active pods every 5 seconds.
            while (true) {
                List<Pod> pods = client.pods().list().getItems();
                for (Pod pod : pods) {
                    updateImageTimer(timer, pod);
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ignored) {
                }
            }
        }

        /**
         * Helper method for the @PodListener.
         * Given a @Timer, register a @TimerTask with it against the images used for a particular pod.
         *
         * @param timer    The @Timer used to register the @TimerTasks
         * @param resource The @Pod to get the image names from.
         */
        private void updateImageTimer(Timer timer, Pod resource) {
            List<Container> containers = resource
                    .getSpec()
                    .getContainers();

            if (containers != null) {
                for (Container container : containers) {

                    final String imageName = container.getImage();

                    // Cancel any existing timers.
                    if (images.containsKey(imageName)) {
                        images.get(imageName).cancel();
                    }

                    // Create a time task that deletes the image.
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            logger.info("Deleting image from artifactory: " + imageName);
                            // TODO: Delete the image from the artifactory
                        }
                    };

                    // Place the image name and a timer in a string for future usage.
                    images.put(container.getImage(), task);

                    // Schedule the task to run 6 months from now.
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, PodListenerService.COUNTDOWN_DAYS);
                    timer.schedule(task, c.getTime());
                }
            }
        }
    }
}