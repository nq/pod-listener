package com.paychex.jmx;

/**
 * MBean used to monitor the health of the FiTS Ach Api
 * 
 * @author mdrum
 *
 */
public class HealthMonitoringImpl implements HealthMonitoringMBean {

	private boolean health = true;
	private String healthReason;
	private boolean busy = false;
	private String busyReason;

	public void setHealth(boolean health) {
		this.health = health;
	}

	public void setHealthReason(String healthReason) {
		this.healthReason = healthReason;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public void setBusyReason(String busyReason) {
		this.busyReason = busyReason;
	}

	public boolean getHealth() {
		return this.health;
	}

	public String getHealthReason() {
		return this.healthReason;
	}

	public boolean getBusy() {
		return this.busy;
	}

	public String getBusyReason() {
		return this.busyReason;
	}

	public String toString() {
		return "\nHealth: " + this.health + "\nHealthReason: "
				+ this.healthReason + "\nBusy: " + this.busy + "\nBusyReason: "
				+ this.busyReason + "\n";
	}

}
