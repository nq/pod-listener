all: docker-build docker-push deploy
gradle-build:
	./gradlew build
docker-build:
	docker build -t "app-prerelease.registry.sdlc.paychex.com/common-pod/podlistener-config" -f "docker/app/release/Config-Dockerfile" .
	docker build -t "app-prerelease.registry.sdlc.paychex.com/common-pod/podlistener" -f "docker/app/release/Dockerfile" .
docker-push:
	docker push "app-prerelease.registry.sdlc.paychex.com/common-pod/podlistener-config"
	docker push "app-prerelease.registry.sdlc.paychex.com/common-pod/podlistener"
deploy: oc-init undeploy
	oc create -f openshift/pod-listener.yml
undeploy: oc-init
	oc delete -f openshift/pod-listener.yml || exit 0
oc-init:
	oc project chilt-playground
