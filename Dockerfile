FROM app-prerelease.registry.sdlc.paychex.com/base/jdk-8-oracle:latest

RUN mkdir /payx
WORKDIR /payx

COPY build/libs/pod-listener-poc.jar /payx/

RUN chmod -R a+rwx /payx

EXPOSE 8080

CMD ["java", "-jar", "./pod-listener-poc.jar"]
